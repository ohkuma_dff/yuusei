$(function(){
  $windowWidth = window.innerWidth;
  if($windowWidth >= 768){
    $('.feat_nav_item').mouseover(function(){ //特集ページのプルダウンメニュー
      navPanel = $(this).children('a');
      navDrop = $(this).children('.feat_nav_drop');
      navPanel.each(function(){
        navPanel.css({
          backgroundColor: '#c00',
          color: '#fff',
        });
      });
      navDrop.each(function(){
        navDrop.stop().slideDown();
      });
    });
    $('.feat_nav_item').mouseout(function(){
      navPanel.each(function(){
        navPanel.css({
          backgroundColor: '#f2f2f2',
          color: '#181818',
        });
      });
      navDrop.each(function(){
        navDrop.stop().slideUp();
      });
    });
  } else {
    $('.feat_nav_item a').click(function(){
      return false;
    });
  }
});
